const express = require('express');
const router = express.Router();
const { createItem, get, soldItem } = require('../controllers/ProductController');

router.post('/', createItem);
router.post('/sold-item', soldItem);
router.get('/', get);

module.exports = router;
