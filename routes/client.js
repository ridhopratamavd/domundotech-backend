const express = require('express');
const router = express.Router();
const { create, get } = require('../controllers/ClientController');

router.post('/', create);
router.get('/', get);

module.exports = router;
