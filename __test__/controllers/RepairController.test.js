const chai = require('chai');
const { stub, restore } = require('sinon');
const sinonChai = require('sinon-chai');
const sinon = require('sinon');
const repairService = require('../../services/RepairService');
const chaiHttp = require('chai-http');
const app = require('../../app');
const { expect } = require('chai');
chai.use(sinonChai);
chai.use(chaiHttp);
chai.use(require('chai-as-promised'));

describe('controllers/repair', () => {
  afterEach(function () {
    restore();
  });

  context('CREATE repair', () => {
    it('should return 500 when service return exception', (done) => {
      const error = new Error();
      error.message = 'internal server error';
      stub(repairService, 'create').throws(error);

      chai.request(app)
        .post('/repair')
        .send({
          receivedBy: 'olang',
          receivedAt: '2020-02-02',
          repairId: '03-03-3030303',
          note: 'some notes'
        })
        .end((_, res) => {
          expect(res.status).to.equal(500);
          expect(res.body).to.have.all.keys('status', 'errors');
          done();
        });
    });

    it('should return 200 when service not returning exception', (done) => {
      sinon.stub(repairService, 'create').callsFake(() => {});
      chai.request(app)
        .post('/repair')
        .send({
          receivedBy: 'olang',
          receivedAt: '2020-02-02',
          repairId: '03-03-3030303',
          note: 'some notes'
        })
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(repairService.create).to.have.been.calledOnce;
          done();
        });
    });
  });

  context('GET repair', () => {
    it('should return data when receive get', (done) => {
      sinon.stub(repairService, 'get').callsFake(() => {});
      chai.request(app)
        .get('/repair')
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(repairService.get).to.have.been.calledOnce;
          done();
        });
    });
  });
});
