const chai = require('chai');
const { stub, restore } = require('sinon');
const sinonChai = require('sinon-chai');
const sinon = require('sinon');
const ProductService = require('../../services/ProductService');
const chaiHttp = require('chai-http');
const app = require('../../app');
const { expect } = require('chai');
chai.use(sinonChai);
chai.use(chaiHttp);
chai.use(require('chai-as-promised'));

describe('controllers/product', () => {
  afterEach(function () {
    restore();
  });

  context('CREATE Product', () => {
    it('should return 500 when service return exception', (done) => {
      const error = new Error();
      error.message = 'internal server error';
      stub(ProductService, 'createItem').throws(error);

      chai.request(app)
        .post('/product')
        .set('content-type', 'application/json')
        .send({ name: 'product', price: 1000, productType: 1, productQuantity: 5 })
        .end((_, res) => {
          expect(res.status).to.equal(500);
          expect(res.body).to.have.all.keys('status', 'errors');
          done();
        });
    });

    it('should return 200 when service not returning exception', (done) => {
      sinon.stub(ProductService, 'createItem').callsFake(() => {});
      chai.request(app)
        .post('/product')
        .send({ name: 'product', price: 1000, productType: 1, productQuantity: 5 })
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(ProductService.createItem).to.have.been.calledOnce;
          done();
        });
    });
  });

  context('GET Product', () => {
    it('should return data when receive get', (done) => {
      sinon.stub(ProductService, 'get').callsFake(() => {});
      chai.request(app)
        .get('/product')
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(ProductService.get).to.have.been.calledOnce;
          done();
        });
    });
  });
});
