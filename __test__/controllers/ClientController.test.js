const chai = require('chai');
const { stub, restore } = require('sinon');
const sinonChai = require('sinon-chai');
const sinon = require('sinon');
const clientService = require('../../services/ClientService');
const chaiHttp = require('chai-http');
const app = require('../../app');
const { expect } = require('chai');
chai.use(sinonChai);
chai.use(chaiHttp);
chai.use(require('chai-as-promised'));

describe('controllers/client', () => {
  afterEach(function () {
    restore();
  });

  context('CREATE repair', () => {
    it('should return 500 when service return exception', (done) => {
      const error = new Error();
      error.message = 'internal server error';
      stub(clientService, 'create').throws(error);

      chai.request(app)
        .post('/client')
        .send({
          name: '',
          address: '',
          phoneNumber: '',
          email: ''
        })
        .end((_, res) => {
          expect(res.status).to.equal(500);
          expect(res.body).to.have.all.keys('status', 'errors');
          done();
        });
    });

    it('should return 200 when service not returning exception', (done) => {
      sinon.stub(clientService, 'create').callsFake(() => {});
      chai.request(app)
        .post('/client')
        .send({
          name: '',
          address: '',
          phoneNumber: '',
          email: ''
        })
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(clientService.create).to.have.been.calledOnce;
          done();
        });
    });
  });

  context('GET repair', () => {
    it('should return data when receive get', (done) => {
      sinon.stub(clientService, 'get').callsFake(() => {});
      chai.request(app)
        .get('/client')
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(clientService.get).to.have.been.calledOnce;
          done();
        });
    });
  });
});
