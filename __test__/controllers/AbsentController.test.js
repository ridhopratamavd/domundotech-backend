const chai = require('chai');
const { stub, restore } = require('sinon');
const sinonChai = require('sinon-chai');
const sinon = require('sinon');
const AbsentService = require('../../services/AbsentService');
const chaiHttp = require('chai-http');
const app = require('../../app');
const { expect } = require('chai');
const { mockRequest } = require('mock-req-res');
chai.use(sinonChai);
chai.use(chaiHttp);
chai.use(require('chai-as-promised'));

describe('controllers/absents', () => {
  afterEach(function () {
    restore();
  });

  context('CREATE ABSENT', () => {
    it('should return 500 when service return exception', (done) => {
      const error = new Error();
      error.message = 'internal server error';
      stub(AbsentService, 'create').throws(error);

      chai.request(app)
        .post('/absent')
        .set('content-type', 'application/json')
        .send({ name: 'rsio' })
        .end((_, res) => {
          expect(res.status).to.equal(500);
          expect(res.body).to.have.all.keys('status', 'errors');
          done();
        });
    });

    it('should return 200 when service not returning exception', (done) => {
      const requestForHTTPCall = mockRequest({
        'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14_0 Mobile/15E148 Safari/604.1'
      });

      sinon.stub(AbsentService, 'create').callsFake(() => {});
      chai.request(app)
        .post('/absent')
        .set(requestForHTTPCall)
        .send({ name: 'rsio' })
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(AbsentService.create).to.have.been.calledOnceWith(requestForHTTPCall['user-agent'], 'rsio');
          done();
        });
    });
  });

  context('GET ABSENT', () => {
    it('should return data when receive get', (done) => {
      sinon.stub(AbsentService, 'get').callsFake(() => {});
      chai.request(app)
        .get('/absent')
        .end((_, res) => {
          expect(res.status).to.equal(200);
          expect(AbsentService.get).to.have.been.calledOnce;
          done();
        });
    });
  });
});
