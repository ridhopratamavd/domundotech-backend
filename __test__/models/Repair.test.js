const chai = require('chai');
const proxyquire = require('proxyquire');
const { sequelize, Sequelize } = require('sequelize-test-helpers');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

describe('models/Repair', () => {
  const { DataTypes } = Sequelize;

  const ClientFactory = proxyquire('../../models/Repair', {
    sequelize: Sequelize, DataTypes
  });

  let Repair;

  before(() => {
    Repair = ClientFactory(sequelize, DataTypes);
  });

  // It's important you do this
  after(() => {
    Repair.init.resetHistory();
  });

  context('column name', () => {
    it('expect called Repair.init with the correct parameters', () => {
      expect(Repair.init).to.have.been.calledWith(
        {
          receivedBy: DataTypes.STRING,
          receivedAt: DataTypes.DATE,
          repairId: DataTypes.STRING,
          note: DataTypes.STRING,
          clientId: DataTypes.INTEGER
        },
        {
          sequelize,
          modelName: 'Repair'
        }
      );
    });
  });

  context('relations', () => {
    const ClientFactory = proxyquire('../../models/Repair', {
      sequelize: Sequelize, DataTypes
    });
    let Client;

    before(() => {
      Client = ClientFactory(sequelize, Sequelize);
      Repair.associate({ Client });
    });

    it('should have relation many to one with client', function () {
      expect(Repair.belongsTo).to.have.been.calledWith(Client, {
        foreignKey: 'clientId',
        as: 'fk_clientId',
        onDelete: 'RESTRICT'
      });
    });
  });
});
