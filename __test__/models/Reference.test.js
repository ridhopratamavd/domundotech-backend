const chai = require('chai');
const proxyquire = require('proxyquire');
const { sequelize, Sequelize } = require('sequelize-test-helpers');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

describe('models/Reference', () => {
  const { DataTypes } = Sequelize;

  const ReferenceFactory = proxyquire('../../models/Reference', {
    sequelize: Sequelize, DataTypes
  });

  let Reference;

  before(() => {
    Reference = ReferenceFactory(sequelize, DataTypes);
  });

  // It's important you do this
  after(() => {
    Reference.init.resetHistory();
  });

  it('expect called Reference.init with the correct parameters', () => {
    expect(Reference.init).to.have.been.calledWith(
      {
        name: DataTypes.STRING,
        type: DataTypes.STRING,
        code: DataTypes.STRING,
        parentId: DataTypes.INTEGER
      },
      {
        sequelize,
        modelName: 'Reference'
      }
    );
  });
});
