const chai = require('chai');
const proxyquire = require('proxyquire');
const { sequelize, Sequelize } = require('sequelize-test-helpers');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

describe('models/RepairDetail', () => {
  const { DataTypes } = Sequelize;

  const RepairDetailFactory = proxyquire('../../models/RepairDetail', {
    sequelize: Sequelize, DataTypes
  });

  let RepairDetail;

  before(() => {
    RepairDetail = RepairDetailFactory(sequelize, DataTypes);
  });

  // It's important you do this
  after(() => {
    RepairDetail.init.resetHistory();
  });

  it('expect called RepairDetail.init with the correct parameters', () => {
    expect(RepairDetail.init).to.have.been.calledWith(
      {
        repairId: DataTypes.INTEGER,
        sparepartType: DataTypes.INTEGER,
        conditions: DataTypes.STRING
      },
      {
        sequelize,
        modelName: 'RepairDetail'
      }
    );
  });
});
