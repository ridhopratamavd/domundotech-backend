const chai = require('chai');
const proxyquire = require('proxyquire');
const { sequelize, Sequelize } = require('sequelize-test-helpers');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

describe('models/Client', () => {
  const { DataTypes } = Sequelize;

  const ClientFactory = proxyquire('../../models/Client', {
    sequelize: Sequelize, DataTypes
  });

  let Client;

  before(() => {
    Client = ClientFactory(sequelize, DataTypes);
  });

  // It's important you do this
  after(() => {
    Client.init.resetHistory();
  });

  it('expect called Client.init with the correct parameters', () => {
    expect(Client.init).to.have.been.calledWith(
      {
        name: DataTypes.STRING,
    address: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: DataTypes.STRING
      },
      {
        sequelize,
        modelName: 'Client'
      }
    );
  });
});
