const chai = require('chai');
const proxyquire = require('proxyquire');
const { sequelize, Sequelize } = require('sequelize-test-helpers');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

describe('models/Product', () => {
  const { DataTypes } = Sequelize;

  const ProductFactory = proxyquire('../../models/Product', {
    sequelize: Sequelize, DataTypes
  });

  let Product;

  before(() => {
    Product = ProductFactory(sequelize, DataTypes);
  });

  // It's important you do this
  after(() => {
    Product.init.resetHistory();
  });

  it('expect called Product.init with the correct parameters', () => {
    expect(Product.init).to.have.been.calledWith(
      {
        name: DataTypes.STRING,
        buyPrice: DataTypes.INTEGER,
        offlineSellPrice: DataTypes.INTEGER,
        onlineSellPrice: DataTypes.INTEGER,
        productType: DataTypes.INTEGER,
        productQuantity: DataTypes.INTEGER,
        description: DataTypes.TEXT,
        notes: DataTypes.TEXT
      },
      {
        sequelize,
        modelName: 'Product'
      }
    );
  });

  context('associations', () => {
    const ReferenceFactory = proxyquire('../../models/Reference', {
      sequelize: Sequelize, DataTypes
    });
    let Reference;

    before(() => {
      Reference = ReferenceFactory(sequelize, Sequelize);
      Product.associate({ Reference });
    });

    it('defined a belongsTo association with Reference', () => {
      expect(Product.belongsTo).to.have.been.calledWith(Reference);
    });
  });
});
