const chai = require('chai');
const proxyquire = require('proxyquire');
const { sequelize, Sequelize } = require('sequelize-test-helpers');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

describe('models/Absent', () => {
  const { DataTypes } = Sequelize;

  const AbsentFactory = proxyquire('../../models/Absent', {
    sequelize: Sequelize, DataTypes
  });

  let Absent;

  before(() => {
    Absent = AbsentFactory(sequelize, DataTypes);
  });

  // It's important you do this
  after(() => {
    Absent.init.resetHistory();
  });

  it('expect called Absent.init with the correct parameters', () => {
    expect(Absent.init).to.have.been.calledWith(
      {
        name: DataTypes.STRING,
        date: DataTypes.DATEONLY,
        time: DataTypes.TIME
      },
      {
        sequelize,
        modelName: 'Absent'
      }
    );
  });
});
