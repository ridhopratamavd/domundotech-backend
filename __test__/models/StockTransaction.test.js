const chai = require('chai');
const proxyquire = require('proxyquire');
const { sequelize, Sequelize } = require('sequelize-test-helpers');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

describe('models/StockTransaction', () => {
  const { DataTypes } = Sequelize;

  const StockTransactionFactory = proxyquire('../../models/StockTransaction', {
    sequelize: Sequelize, DataTypes
  });

  let StockTransaction;

  before(() => {
    StockTransaction = StockTransactionFactory(sequelize, DataTypes);
  });

  // It's important you do this
  after(() => {
    StockTransaction.init.resetHistory();
  });

  it('expect called StockTransaction.init with the correct parameters', () => {
    expect(StockTransaction.init).to.have.been.calledWith(
      {
        transactionType: DataTypes.INTEGER,
        stockId: DataTypes.INTEGER,
        buyPrice: DataTypes.INTEGER,
        sellPrice: DataTypes.INTEGER,
        profit: DataTypes.INTEGER,
        source: DataTypes.STRING,
        paymentType: DataTypes.INTEGER,
        paymentStatus: DataTypes.INTEGER,
        quantity: DataTypes.INTEGER,
        note: DataTypes.TEXT,
        invoiceNumber: DataTypes.STRING,
        ovoIn: DataTypes.INTEGER,
        ovoOut: DataTypes.INTEGER,
        tokoPointIn: DataTypes.INTEGER,
        tokoPointOut: DataTypes.INTEGER,
        bcaIn: DataTypes.INTEGER,
        bcaOut: DataTypes.INTEGER,
        ovoBalance: DataTypes.INTEGER,
        tokoPointBalance: DataTypes.INTEGER,
        bcaBalance: DataTypes.INTEGER,
        cashIn: DataTypes.INTEGER,
        cashOut: DataTypes.INTEGER,
        cashBalance: DataTypes.INTEGER
      },
      {
        sequelize,
        modelName: 'StockTransaction'
      }
    );
  });

  context('associations', () => {
    const ReferenceFactory = proxyquire('../../models/Reference', {
      sequelize: Sequelize, DataTypes
    });
    let Reference;

    before(() => {
      Reference = ReferenceFactory(sequelize, Sequelize);
      StockTransaction.associate({ Reference });
    });

    it('defined a belongsTo association with Reference and its referenced column', () => {
      expect(StockTransaction.belongsTo).to.have.been.calledWith(Reference, { foreignKey: 'transactionType', onDelete: 'CASCADE' });
      expect(StockTransaction.belongsTo).to.have.been.calledWith(Reference, { foreignKey: 'paymentStatus', onDelete: 'CASCADE' });
      expect(StockTransaction.belongsTo).to.have.been.calledWith(Reference, { foreignKey: 'paymentType', onDelete: 'CASCADE' });
    });
  });
});
