const StockTransactionService = require('../../services/StockTransactionService');
const StockTransactionRepository = require('../../repositories/StockTransactionRepository');

const chai = require('chai');
const sinon = require('sinon');
const StockTransaction = require('../../models/StockTransaction');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('services/StockTransactionService', () => {
  afterEach(function () {
    sinon.restore();
  });

  it('expect not to throws error when success creating StockTransaction record', async () => {
    sinon.stub(StockTransactionRepository, 'create').callsFake(() => {
      return {};
    });

    await StockTransactionService.create({
      transactionType: 1,
      stockId: 1,
      buyPrice: 1,
      sellPrice: 1,
      profit: 1,
      source: 'toko roti',
      paymentType: 1,
      paymentStatus: 1,
      quantity: 1,
      note: 'ini catatan yang berupa text',
      invoiceNumber: '',
      ovoIn: 0,
      ovoOut: 0,
      tokoPointIn: 0,
      tokoPointOut: 0,
      bcaIn: 0,
      bcaOut: 0,
      ovoBalance: 0,
      tokoPointBalance: 0,
      bcaBalance: 0,
      cashIn: 0,
      cashOut: 0,
      cashBalance: 0
    });

    expect(StockTransactionRepository.create).to.have.been.called;
  });
});
