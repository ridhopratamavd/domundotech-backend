const RepairService = require('../../services/RepairService');
const RepairRepository = require('../../repositories/RepairRepository');

const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('services/RepairService', () => {
  afterEach(function () {
    sinon.restore();
  });

  it('expect not to throws error when success creating Repair record', async () => {
    sinon.stub(RepairRepository, 'create').callsFake(() => {
      return { _options: { isNewRecord: true } };
    });

    const result = await RepairService.create({ receiveBy: 'olang', repairId: '09-09-092021', note: 'layar rusak' });

    expect(result).to.have.all.keys('status', 'result');
  });
});
