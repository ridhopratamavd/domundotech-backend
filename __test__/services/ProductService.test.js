const ProductService = require('../../services/ProductService');
const ProductRepository = require('../../repositories/ProductRepository');

const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('services/ProductService', () => {
  afterEach(function () {
    sinon.restore();
  });

  it('expect not to throws error when success creating Product record', async () => {
    sinon.stub(ProductRepository, 'create').callsFake(() => {
      return { _options: { isNewRecord: true } };
    });

    const result = await ProductService.createItem({ name: 'name of stock', price: 3000, stockType: 1, stockQuantity: 35 });

    expect(result).to.have.all.keys('status', 'result');
  });
});
