const AbsentService = require('../../services/AbsentService');
const AbsentRepository = require('../../repositories/AbsentRepository');

const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();
const mockDate = require('mockdate');

chai.use(require('chai-as-promised'));

describe('services/absents', () => {
  afterEach(function () {
    sinon.restore();
  });

  context('CREATE ABSENT', () => {
    const invalidHttpHeaderRequest = 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14_0 Mobile/15E148 Safari/604.1';

    const validHttpHeaderRequest = 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1';

    it('expect to throws error when header is invalid ', async () => {
      mockDate.set(moment('03-29-2021', 'MM-DD-YYYY').toDate()); // senin
      await expect(AbsentService.create(invalidHttpHeaderRequest, 'riooo'))
        .to.eventually.be.rejected
        .and.be.an.instanceOf(Error)
        .then(function (error) {
          expect(error).to.have.property('message', 'invalid days nor invalid user');
        });
    });

    it('expect not to throws error when header is valid and valid days', async () => {
      mockDate.set(moment('03-29-2021', 'MM-DD-YYYY').toDate()); // senin
      sinon.stub(AbsentRepository, 'create').callsFake(() => {
        return [{}, true];
      });

      const result = await AbsentService.create(validHttpHeaderRequest, 'rio');

      expect(result).to.have.all.keys('status', 'result');
    });

    it('expect to throws error when header is valid and invalidDays', async () => {
      mockDate.set(moment('03-28-2021', 'MM-DD-YYYY').toDate()); // minggu

      await expect(AbsentService.create(invalidHttpHeaderRequest, 'riooo'))
        .to.eventually.be.rejected
        .and.be.an.instanceOf(Error)
        .then(function (error) {
          expect(error).to.have.property('message', 'invalid days nor invalid user');
        });
    });
  });

  context('GET ABSENT', () => {
    it('expect get all data', async () => {
      sinon.stub(AbsentRepository, 'getAll').callsFake(() => {
        return [];
      });

      const result = await AbsentService.get();

      expect(result).to.be.empty;
    });
  });
});
