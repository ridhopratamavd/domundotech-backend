const ReferenceService = require('../../services/ReferenceService');
const ReferenceRepository = require('../../repositories/ReferenceRepository');

const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('services/ReferenceService', () => {
  afterEach(function () {
    sinon.restore();
  });

  it('expect not to throws error when success creating reference record', async () => {
    sinon.stub(ReferenceRepository, 'create').callsFake(() => {
      return [{}, true];
    });

    const result = await ReferenceService.create({ type: 'STOCK_TYPE', name: 'ACCESSORIES' });

    expect(result).to.have.all.keys('status', 'result');
  });

  context('GET Reference', () => {
    it('expect get all data when no query params supplied', async () => {
      sinon.stub(ReferenceRepository, 'getAll').callsFake(() => {
        return [];
      });
      const fakeRequest = { query: {} };

      const result = await ReferenceService.getAll(fakeRequest);

      expect(result).to.be.empty;
    });

    it('expect get all data with specific type when query params contain reference type', async () => {
      sinon.stub(ReferenceRepository, 'findWithReferenceType').callsFake(() => {
        return [];
      });
      const fakeRequest = { query: { reference_type: 'reference_type_fake' } };

      const result = await ReferenceService.getAllWithReferenceType(fakeRequest);

      expect(result).to.be.empty;
    });
  });
});
