const ClientService = require('../../services/ClientService');
const ClientRepository = require('../../repositories/ClientRepository');

const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('services/ClientService', () => {
  afterEach(function () {
    sinon.restore();
  });

  it('expect not to throws error when success creating Client record', async () => {
    sinon.stub(ClientRepository, 'create').callsFake(() => {
      return [{}, true];
    });

    const result = await ClientService.create({
      name: 'client name',
      address: 'jala manyar',
      phoneNumber: '0802398019280549812',
      email: 'mayr@gmail.com'
    });

    expect(result).to.have.all.keys('status', 'result');
  });

  it('expect get all data', async () => {
    sinon.stub(ClientRepository, 'getAll').callsFake(() => {
      return [];
    });

    const result = await ClientService.get();

    expect(result).to.be.empty;
  });
});
