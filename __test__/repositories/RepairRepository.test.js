const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const { makeMockModels } = require('sequelize-test-helpers');
const expect = chai.expect;

describe('repository/RepairRepository', () => {
  const Repair = { create: stub(), findAll: stub() };
  const mockModels = makeMockModels({ Repair });

  const RepairRepository = proxyquire('./../../repositories/RepairRepository', {
    '../models': mockModels
  });

  before(() => {
    Repair.create.resolves(mockModels);
    Repair.findAll.resolves(mockModels);
  });

  after(() => {
    resetHistory();
  });

  it('expect call create when create', async () => {
    const oneRepair = {
      name: 'fake name',
      address: 'fake address',
      email: 'fake email',
      phoneNumber: '09127019341'
    };

    await RepairRepository.create(oneRepair);

    expect(Repair.create).to.have.been.called;
  });

  it('expect call findAll when getAll', async () => {
    await RepairRepository.getAll();

    expect(Repair.findAll).to.have.been.called;
  });
});
