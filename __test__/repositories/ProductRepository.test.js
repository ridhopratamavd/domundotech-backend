const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const { makeMockModels } = require('sequelize-test-helpers');
const expect = chai.expect;

describe('repository/ProductRepository', () => {
  const Product = { create: stub() };
  const mockModels = makeMockModels({ Product });

  const ProductRepository = proxyquire('./../../repositories/ProductRepository', {
    '../models': mockModels
  });

  before(() => {
    Product.create.resolves(mockModels);
  });

  after(() => {
    resetHistory();
  });

  it('expect call createItem when createItem', async () => {
    const oneStock = {
      name: 'name of stock',
      price: 185000,
      stockType: 1,
      stockQuantity: 25,
      buy: 90000
    };
    await ProductRepository.create(oneStock);
    expect(Product.create).to.have.been.called;
  });
});
