const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const { makeMockModels } = require('sequelize-test-helpers');
const expect = chai.expect;

describe('repository/StockTransactionRepository', () => {
  const StockTransaction = { create: stub() };
  const mockModels = makeMockModels({ StockTransaction });

  const StockTransactionRepository = proxyquire('./../../repositories/StockTransactionRepository', {
    '../models': mockModels
  });

  before(() => {
    StockTransaction.create.resolves(mockModels);
  });

  after(() => {
    resetHistory();
  });

  it('expect call create when create', async () => {
    const oneStockTransaction = {
      transactionType: 1,
      stockId: 1,
      buyPrice: 2000,
      sellPrice: 3000,
      profit: 1000,
      source: 'fake',
      paymentType: 1,
      paymentStatus: 1,
      quantity: 90,
      note: ''
    };
    await StockTransactionRepository.create(oneStockTransaction);
    expect(StockTransaction.create).to.have.been.called;
  });
});
