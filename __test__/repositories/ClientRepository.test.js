const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const { makeMockModels } = require('sequelize-test-helpers');
const expect = chai.expect;

describe('repository/ClientRepository', () => {
  const Client = { findOrCreate: stub(), findAll: stub() };
  const mockModels = makeMockModels({ Client });

  const ClientRepository = proxyquire('./../../repositories/ClientRepository', {
    '../models': mockModels
  });

  before(() => {
    Client.findOrCreate.resolves(mockModels);
    Client.findAll.resolves(mockModels);
  });

  after(() => {
    resetHistory();
  });

  it('expect call create when create', async () => {
    const oneClient = {
      name: 'fake name',
      address: 'fake address',
      email: 'fake email',
      phoneNumber: '09127019341'
    };

    await ClientRepository.create(oneClient);

    expect(Client.findOrCreate).to.have.been.called;
  });

  it('expect call findAll when getAll', async () => {
    await ClientRepository.getAll();

    expect(Client.findAll).to.have.been.called;
  });
});
