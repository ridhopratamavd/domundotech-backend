const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const { makeMockModels } = require('sequelize-test-helpers');
const expect = chai.expect;

describe('repository/ReferenceRepository', () => {
  const Reference = { findOrCreate: stub(), findAll: stub() };
  const mockModels = makeMockModels({ Reference });

  const ReferenceRepository = proxyquire('./../../repositories/ReferenceRepository', {
    '../models': mockModels
  });

  before(() => {
    Reference.findOrCreate.resolves(mockModels);
    Reference.findAll.resolves({});
  });

  after(() => {
    resetHistory();
  });

  it('expect call create when create', async () => {
    await ReferenceRepository.create({ type: 'STOCK_TYPE', name: 'ACCESSORIES', code: 'AC' });
    expect(Reference.findOrCreate).to.have.been.called;
  });

  it('expect call findAll when get', async () => {
    await ReferenceRepository.getAll();
    expect(Reference.findAll).to.have.been.called;
  });
});
