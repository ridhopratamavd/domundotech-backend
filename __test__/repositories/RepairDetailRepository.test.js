const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const { makeMockModels } = require('sequelize-test-helpers');
const expect = chai.expect;

describe('repository/RepairDetailRepository', () => {
  const RepairDetail = { create: stub() };
  const mockModels = makeMockModels({ RepairDetail });

  const RepairDetailRepository = proxyquire('./../../repositories/RepairDetailRepository', {
    '../models': mockModels
  });

  before(() => {
    RepairDetail.create.resolves(mockModels);
  });

  after(() => {
    resetHistory();
  });

  it('expect call create when create', async () => {
    const oneRepairDetail = {
      repairId: 1,
      sparepartType: 1,
      conditions: 'sip'
    };
    await RepairDetailRepository.create(oneRepairDetail);
    expect(RepairDetail.create).to.have.been.called;
  });
});
