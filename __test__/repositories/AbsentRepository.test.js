const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const { makeMockModels } = require('sequelize-test-helpers');
const expect = chai.expect;
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();
const mockDate = require('mockdate');

describe('repository/AbsentRepository', () => {
  const Absent = { findOrCreate: stub(), findAll: stub() };
  const mockModels = makeMockModels({ Absent });

  const AbsentRepository = proxyquire('./../../repositories/AbsentRepository', {
    '../models': mockModels
  });

  before(() => {
    Absent.findOrCreate.resolves([{}, true]);
    Absent.findAll.resolves({});
  });

  after(() => {
    resetHistory();
  });

  it('expect call findOrCreate when create', async () => {
    await AbsentRepository.create('rio');
    expect(Absent.findOrCreate).to.have.been.called;
  });

  it('expect call findAll when get', async () => {
    const mockedDate = moment('03-29-2021', 'MM-DD-YYYY');
    mockDate.set(mockedDate.toDate()); // senin
    await AbsentRepository.getAll();
    expect(Absent.findAll).to.have.been.called;
    // expect(Absent.findAll).to.have.been.calledWithMatch({ where: { date: mockedDate } });
  });
});
