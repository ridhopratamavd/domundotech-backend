'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StockTransaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
      StockTransaction.belongsTo(models.Reference, { foreignKey: 'transactionType', onDelete: 'CASCADE' });
      StockTransaction.belongsTo(models.Reference, { foreignKey: 'paymentStatus', onDelete: 'CASCADE' });
      StockTransaction.belongsTo(models.Reference, { foreignKey: 'paymentType', onDelete: 'CASCADE' });
      StockTransaction.belongsTo(models.Product, { foreignKey: 'stockId', onDelete: 'CASCADE' });
    }
  }
  StockTransaction.init({
    transactionType: DataTypes.INTEGER,
    stockId: DataTypes.INTEGER,
    buyPrice: DataTypes.INTEGER,
    sellPrice: DataTypes.INTEGER,
    profit: DataTypes.INTEGER,
    source: DataTypes.STRING,
    paymentType: DataTypes.INTEGER,
    paymentStatus: DataTypes.INTEGER,
    quantity: DataTypes.INTEGER,
    note: DataTypes.TEXT,
    invoiceNumber: DataTypes.STRING,
    ovoIn: DataTypes.INTEGER,
    ovoOut: DataTypes.INTEGER,
    tokoPointIn: DataTypes.INTEGER,
    tokoPointOut: DataTypes.INTEGER,
    bcaIn: DataTypes.INTEGER,
    bcaOut: DataTypes.INTEGER,
    ovoBalance: DataTypes.INTEGER,
    tokoPointBalance: DataTypes.INTEGER,
    bcaBalance: DataTypes.INTEGER,
    cashIn: DataTypes.INTEGER,
    cashOut: DataTypes.INTEGER,
    cashBalance: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'StockTransaction'
  });
  return StockTransaction;
};
