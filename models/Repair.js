'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Repair extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
      Repair.belongsTo(models.Client, {
        foreignKey: 'clientId',
        as: 'fk_clientId',
        onDelete: 'RESTRICT'
      });
    }
  };
  Repair.init({
    receivedBy: DataTypes.STRING,
    receivedAt: DataTypes.DATE,
    repairId: DataTypes.STRING,
    note: DataTypes.STRING,
    clientId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Repair'
  });
  return Repair;
};
