'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RepairDetail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
    }
  };
  RepairDetail.init({
    repairId: DataTypes.INTEGER,
    sparepartType: DataTypes.INTEGER,
    conditions: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RepairDetail'
  });
  return RepairDetail;
};
