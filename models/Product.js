'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
      Product.belongsTo(models.Reference, { foreignKey: 'productType' });
    }
  }
  Product.init({
    name: DataTypes.STRING,
    buyPrice: DataTypes.INTEGER,
    offlineSellPrice: DataTypes.INTEGER,
    onlineSellPrice: DataTypes.INTEGER,
    productType: DataTypes.INTEGER,
    productQuantity: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    notes: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Product'
  });
  return Product;
};
