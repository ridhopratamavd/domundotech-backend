exports.RECORD_ALREADY_CREATED = {
  message: 'record already created'
};

exports.CREATED = {
  message: 'record created'
};

exports.INVALID_REQUEST = {
  message: 'invalid request'
};

exports.BAD_REQUEST = {
  message: 'bad request'
};

exports.HTTP_STATUS_OK = 200;
exports.HTTP_STATUS_INVALID_REQUEST = 409;
exports.HTTP_STATUS_INTERNAL_SERVER_ERROR = 500;
