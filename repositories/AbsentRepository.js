const { Absent } = require('../models');
const { Sequelize } = require('sequelize');
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();

module.exports = class AbsentRepository {
    static create = async (name) => {
        const Op = Sequelize.Op;
        const NOW_DATE = moment(new Date()).format('YYYY-MM-DD');
        const NOW_TIME = moment(new Date()).format('HH:mm:ss');
        return await Absent.findOrCreate({
            where: {
                name: name,
                date: {
                    [Op.eq]: NOW_DATE
                }
            },
            defaults: {
                name,
                date: NOW_DATE,
                time: NOW_TIME
            },
            raw: true
        });
    };

    static getAll = async () => {
        return await Absent.findAll({
            where: {
                date: moment(new Date())
              }
        });
    }
};
