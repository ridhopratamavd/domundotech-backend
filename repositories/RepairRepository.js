const { Repair } = require('../models');
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();

module.exports = class RepairRepository {
    static create = async (data) => {
        return await Repair.create(data);
    };

    static getAll = async () => {
        return await Repair.findAll({
            include: ["fk_clientId"]
        });
    }
};
