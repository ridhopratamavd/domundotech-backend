const { Client } = require('../models');

module.exports = class ClientRepository {
    static create = async (client) => {
        return await Client.findOrCreate({
            where: {
                name: client.name,
                address: client.address,
                phoneNumber: client.phoneNumber,
                email: client.email
            }
        });
    };

    static getAll = async () => {
        return await Client.findAll({ include: 'Repairs' });
    };
};
