const { StockTransaction } = require('../models');

module.exports = class StockTransactionRepository {
    static create = async (stockTransaction) => {
        return await StockTransaction.create(stockTransaction);
    };

    static findLatestRecord = async () => {
        return await StockTransaction.findAll({
            raw: true,
            limit: 1,
            order: [ [ 'createdAt', 'DESC' ]],
        });
    }
};
