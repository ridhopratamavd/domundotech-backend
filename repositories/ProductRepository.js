const { Product } = require('../models');

module.exports = class ProductRepository {
  static create = async (product) => {
    return await Product.create(product);
  };

  static getAll = async () => {
    return await Product.findAll({ include: 'Reference' });
  };

  static getProductById = async (productId) => {
    return await Product.findOne({ where: { id: productId } });
  };

  static decrementProductQuantity = async (product, soldQuantity) => {
    let result = false;
    await Product.update({
      productQuantity: product.productQuantity - soldQuantity
    }, {
      where: {
        id: product.id
      }
    })
      .then(updateResult => {
        result = updateResult[0] === 1;
      })
      .catch(error => {
        console.log(error);
        result = false;
      });
    return result;
  };
};
