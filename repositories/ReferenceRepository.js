const { Reference } = require('../models');

module.exports = class ReferenceRepository {
    static create = async (reference) => {
        return await Reference.findOrCreate({
            where: {
                name: reference.name,
                type: reference.type,
                code: reference.code,
            }, default: {
                parentId: null
            }
        });
    };

    static getAll = async () => {
        return await Reference.findAll();
    };

    static findWithReferenceType = async (referenceType) => {
        return await Reference.findAll({
            where: {
                type: referenceType
            }
        });
    };

    static findWithReferenceTypeAndCode = async (referenceType, code) => {
        return await Reference.findOne({where: {
                type: referenceType,
                code
            }});
    }
};
