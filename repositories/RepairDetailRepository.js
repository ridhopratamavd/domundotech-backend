const { RepairDetail } = require('../models');
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();

module.exports = class RepairDetailRepository {
    static create = async (data) => {
        return await RepairDetail.create(data);
    };

    static getAll = async () => {
        return await RepairDetail.findAll();
    }
};
