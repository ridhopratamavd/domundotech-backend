'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('References', [
      {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'HDD',
        code: '1',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'RAM',
        code: '2',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'BATTERY',
        code: '3',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'ADAPTER',
        code: '4',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'CD-ROM',
        code: '5',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'LCD/LED',
        code: '6',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'CAMERA',
        code: '7',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'WIFI & BLUETOOTH',
        code: '8',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'KEYBOARD',
        code: '9',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'TOUCHPAD',
        code: '10',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'SPEAKER',
        code: '11',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'MICROPHONE',
        code: '12',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'BAUT',
        code: '13',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'I/O',
        code: '14',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'FISIK',
        code: '15',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        type: 'REPAIR_SPAREPART_TYPE',
        name: 'OTHERS',
        code: '16',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
