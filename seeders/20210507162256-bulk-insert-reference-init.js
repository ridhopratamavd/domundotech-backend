'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('Reference', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('References', [
      {
        type: 'PRODUCT_TYPE',
        name: 'ACCESSORIES',
        code: 'ACC',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'LCD LAPTOP',
        code: 'LCD_LAPTOP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'LCD HP',
        code: 'LCD_HP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'KEYBOARD LAPTOP',
        code: 'KEYBOARD_LAPTOP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'KEYBOARD EXTERNAL',
        code: 'KEYBOARD_EXTERNAL',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'SPEAKER EXTERNAL',
        code: 'SPEAKER_EXTERNAL',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'HARDISK',
        code: 'HDD',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'RAM LAPTOP',
        code: 'RAM_LAPTOP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'RAM PC',
        code: 'RAM_PC',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'MOUSE',
        code: 'MOUSE',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'CHARGER LAPTOP',
        code: 'CHARGER_LAPTOP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'SOLID STATE DISK',
        code: 'SSD',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'LICENSE SOFTWARE',
        code: 'SOFTWARE',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'CASING PC',
        code: 'CASING_PC',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'MOTHERBOARD PC',
        code: 'MOTHERBOARD_PC',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'DVD RW',
        code: 'ACC',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'PROCESSOR',
        code: 'PROCESSOR',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'VGA',
        code: 'VGA_CARD',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'STAVOLT',
        code: 'STAVOLT',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'SCREEN GUARD LAPTOP',
        code: 'SCREEN_GUARD_LAPTOP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'ROUTER',
        code: 'ROUTER',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'POWER SUPPLY',
        code: 'PSU',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'LAPTOP',
        code: 'LAPTOP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'HDD CADDY',
        code: 'ACC',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'FLASHDISK',
        code: 'FLASHDISK',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'PRODUCT_TYPE',
        name: 'BATTERY LAPTOP',
        code: 'BATTERY_LAPTOP',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'TRANSACTION_TYPE',
        name: 'OPERATIONAL',
        code: 'OPERATIONAL',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'TRANSACTION_TYPE',
        name: 'CASH IN',
        code: 'CASH_IN',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        type: 'TRANSACTION_TYPE',
        name: 'CASH OUT',
        code: 'CASH_OUT',
        parentId: null,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
