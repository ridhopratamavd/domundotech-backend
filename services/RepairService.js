const repository = require('./../repositories/RepairRepository');

module.exports = class RepairService {
  static create = async (reference) => {
    const result = await repository.create(reference);
    if (result._options.isNewRecord === true) {
      return {"status" : {"code": "00", "description": "Success"}, result: [result.dataValues]}
    }
    return {"status" : {"code": "01", "description": "Already exist"}, result: [result]}
  };

  static get = async () => {
        return await repository.getAll();
  };
};
