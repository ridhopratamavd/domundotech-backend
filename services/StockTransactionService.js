const repository = require('../repositories/StockTransactionRepository');
const referenceService = require('../services/ReferenceService');

module.exports = class StockTransactionService {
  static create = async (stock) => {
    return await repository.create(stock);
  };

  static createBuyItem = async (product, buyQuantity, note, tokoPointIn, tokoPointOut, ovoIn, ovoOut, bcaIn, bcaOut, cashIn, cashOut) => {
    const cashOutTransactionTypeId = await referenceService.getReferenceByreferenceTypeAndCode('TRANSACTION_TYPE', 'CASH_OUT');
    let latestStockTransaction = await repository.findLatestRecord();
    let latestTokoPointBalance = 0;
    let latestOvoBalance = 0;
    let latestBcaBalance = 0;
    let latestCashBalance = 0;
    if (latestStockTransaction[0] !== undefined ) {
      latestTokoPointBalance = latestStockTransaction[0].tokoPointBalance;
      latestOvoBalance = latestStockTransaction[0].ovoBalance;
      latestBcaBalance = latestStockTransaction[0].bcaBalance;
      latestCashBalance = latestStockTransaction[0].cashBalance;
    }
    let tokoPointBalance = this.calculateBalance(latestTokoPointBalance, tokoPointIn, tokoPointOut);
    let ovoBalance = this.calculateBalance(latestOvoBalance, ovoIn, ovoOut);
    let bcaBalance = this.calculateBalance(latestBcaBalance, bcaIn, bcaOut);
    let cashBalance = this.calculateBalance(latestCashBalance, cashIn, cashOut);

    return await this.create({
      transactionType: cashOutTransactionTypeId,
      stockId: product.id,
      buyPrice: product.buyPrice,
      quantity: buyQuantity,
      sellPrice: "",
      profit: "",
      source: "",
      paymentType: "",
      paymentStatus: "",
      note,
      tokoPointIn,
      tokoPointOut,
      ovoIn,
      ovoOut,
      bcaIn,
      bcaOut,
      tokoPointBalance,
      ovoBalance,
      bcaBalance,
      cashIn,
      cashOut,
      cashBalance
    });
  };

  static createSoldItem = async (product, soldQuantity, isFromOfflineStore, invoiceNumber, note, tokoPointIn, tokoPointOut, ovoIn, ovoOut, bcaIn, bcaOut, cashIn, cashOut) => {
    const cashInTransactionTypeId = await referenceService.getReferenceByreferenceTypeAndCode('TRANSACTION_TYPE', 'CASH_IN');
    const sellPrice = isFromOfflineStore ? product.offlineSellPrice: product.onlineSellPrice;
    let latestStockTransaction = await repository.findLatestRecord();
    let latestTokoPointBalance = 0;
    let latestOvoBalance = 0;
    let latestBcaBalance = 0;
    let latestCashBalance = 0;
    if (latestStockTransaction[0] !== undefined ) {
      latestTokoPointBalance = latestStockTransaction[0].tokoPointBalance;
      latestOvoBalance = latestStockTransaction[0].ovoBalance;
      latestBcaBalance = latestStockTransaction[0].bcaBalance;
      latestCashBalance = latestStockTransaction[0].cashBalance;
    }
    let tokoPointBalance = this.calculateBalance(latestTokoPointBalance, tokoPointIn, tokoPointOut);
    let ovoBalance = this.calculateBalance(latestOvoBalance, ovoIn, ovoOut);
    let bcaBalance = this.calculateBalance(latestBcaBalance, bcaIn, bcaOut);
    let cashBalance = this.calculateBalance(latestCashBalance, cashIn, cashOut);

    return await this.create({
      transactionType: cashInTransactionTypeId,
      stockId: product.id,
      buyPrice: product.buyPrice,
      sellPrice: sellPrice,
      profit: sellPrice - product.buyPrice,
      quantity: soldQuantity,
      invoiceNumber,
      note,
      tokoPointIn,
      tokoPointOut,
      ovoIn,
      ovoOut,
      bcaIn,
      bcaOut,
      tokoPointBalance,
      ovoBalance,
      bcaBalance,
      cashIn,
      cashOut,
      cashBalance
    });
  };

  static calculateBalance (latestBalance, debit, credit) {
    let result = latestBalance;
    result = (debit !== undefined) ? result + debit : result;
    result = (credit !== undefined) ? result - credit : result;
    return result;
  }
};
