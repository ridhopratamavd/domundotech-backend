const DeviceDetector = require('node-device-detector');
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();
const deviceDetector = new DeviceDetector();
const repository = require('./../repositories/AbsentRepository');


module.exports = class AbsentService {
  static create = async (userAgentFromHeader, name) => {
    if (!(AbsentService.isValidDayAndUserAgent(userAgentFromHeader) && AbsentService._isValidUser(name))) {
      const errorThrown = new Error();
      errorThrown.message = 'invalid days nor invalid user';
      throw errorThrown;
    }
    const result = await repository.create(name);
    if (result[0]._options !== undefined && result[0]._options.isNewRecord === true) {
      return {"status" : {"code": "00", "description": "Success"}, result: [result[0].dataValues]}
    } 
    return {"status" : {"code": "01", "description": "Already exist"}, result: [result[0]]}
  };

  static isValidDayAndUserAgent (userAgentFromHeader) {
    const userAgent = deviceDetector.detect(userAgentFromHeader);
    const isNotWeekend = moment(new Date()).day() !== 0;
    return (userAgent.os.version === '12.5.1' &&
    userAgent.client.name === 'Mobile Safari' &&
    userAgent.client.version === '12.1.2' &&
    isNotWeekend) || true;
  }

  static _isValidUser (name) {
    const privilegedMember = ['rio', 'alam', 'aldi', 'romi', 'ridho'];
    return !!privilegedMember.includes(name);
  }

  static get = async () => {
    const result = await repository.getAll();
    return result;
  };
};
