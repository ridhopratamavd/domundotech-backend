const repository = require('../repositories/ProductRepository');
const stockTransactionService = require('../services/StockTransactionService');

module.exports = class ProductService {
  static createItem = async (body) => {
    const { tokoPointIn,
      tokoPointOut,
      ovoIn,
      ovoOut,
      bcaIn,
      cashIn,
      cashOut,
      bcaOut, ...stock} =  body;
    const result = await repository.create(stock);
    if (result._options.isNewRecord === true) {
      await stockTransactionService.createBuyItem(
        product,
        buyQuantity,
        result.dataValues,
        stock.productQuantity,
        stock.notes,
        tokoPointIn,
        tokoPointOut,
        ovoIn,
        ovoOut,
        bcaIn,
        bcaOut,
        cashIn,
        cashOut
        );
      return {"status" : {"code": "00", "description": "Success"}, result: [result.dataValues]}
    }
    return {"status" : {"code": "01", "description": "Already exist"}, result: [result]}
  };

  static soldItem = async (body) => {
    const { selectedProduct: productId,
      soldQuantity,
      isFromOfflineStore,
      invoiceNumber,
      note,
      tokoPointIn,
      tokoPointOut,
      ovoIn,
      ovoOut,
      bcaIn,
      bcaOut,
      cashIn,
      cashOut
    } = body;
    const product = await repository.getProductById(productId);

    let result = false;
    if (product.productQuantity >= soldQuantity) {
      result = await repository.decrementProductQuantity(product, soldQuantity);
    }

    if (result) {
      await stockTransactionService.createSoldItem(product, soldQuantity, isFromOfflineStore, invoiceNumber, note, tokoPointIn, tokoPointOut, ovoIn, ovoOut, bcaIn, bcaOut, cashIn, cashOut);
      return {"status" : {"code": "00", "description": "Success"}, result: [result]}
    }
    return {"status" : {"code": "06", "description": "General Error"}, result: [result]}
  };

  // data returned not represent as actual result from database
  static get = async () => {
    const result = await repository.getAll();
    const response = result.map( product => {
      const { id, name,
        buyPrice,
        offlineSellPrice,
        onlineSellPrice,
        productQuantity,
        description,
        notes,
        createdAt,
        updatedAt,
        Reference
      } = product;

      return {
        id,
        name: Reference.name.concat(' ').concat(name),
        buyPrice,
        offlineSellPrice,
        onlineSellPrice,
        productQuantity,
        description,
        notes,
        createdAt,
        updatedAt
      }

    });
    return response;
  };
};
