const repository = require('./../repositories/ReferenceRepository');

module.exports = class ReferenceService {
  static create = async (reference) => {
    const result = await repository.create(reference);
    if (result[0]._options !== undefined && result[0]._options.isNewRecord === true) {
      return {"status" : {"code": "00", "description": "Success"}, result: [result[0].dataValues]}
    } 
    return {"status" : {"code": "01", "description": "Already exist"}, result: [result[0]]}
  };

  static getAll = () => {
    return repository.getAll();
  };

  static getAllWithReferenceType = async (reference_type) => {
    return await repository.findWithReferenceType(reference_type);
  };

  static getReferenceByreferenceTypeAndCode = async (referenceType, code) => {
    return await repository.findWithReferenceTypeAndCode(referenceType, code).then( result => result.id);
  };
};
