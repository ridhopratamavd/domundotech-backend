const repository = require('./../repositories/ClientRepository');

module.exports = class ClientService {
  static create = async (client) => {
    const result = await repository.create(client);
    if (result[0]._options !== undefined && result[0]._options.isNewRecord === true) {
      return {"status" : {"code": "00", "description": "Success"}, result: [result[0].dataValues]}
    }
    return {"status" : {"code": "01", "description": "Already exist"}, result: [result[0]]}
  };

  static get = async () => {
        return await repository.getAll();
  };
};
