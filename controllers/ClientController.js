const { HTTP_STATUS_OK, HTTP_STATUS_INTERNAL_SERVER_ERROR } = require ('../constants/Response');

const ClientService =  require('../services/ClientService');
module.exports = class ClientController {
  static create = async (req, res, next) => {
    try {
      const result = await ClientService.create(req.body);
      res.status(HTTP_STATUS_OK);
      res.json(result);
    } catch (error) {
      next(error);
      res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR);
      res.json({"status" : {"code": "06", "description": "General Error"}, "errors": [{"message": "Please contact the Administrator"}]});
    }
  };

  static get = async (_, res, next) => {
    try {
      const data = await ClientService.get();
      res.status(HTTP_STATUS_OK);
      res.json(data);
    } catch (error) {
      next(error);
    }
  };

};
