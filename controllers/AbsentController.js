const { INVALID_REQUEST, HTTP_STATUS_OK, HTTP_STATUS_INVALID_REQUEST, CREATED, HTTP_STATUS_INTERNAL_SERVER_ERROR } = require ('../constants/Response');

const AbsentService =  require('../services/AbsentService');
module.exports = class AbsentController {
  static create = async (req, res, next) => {
    try {
      const result = await AbsentService.create(req.headers['user-agent'], req.body.name);
      res.status(HTTP_STATUS_OK);
      res.json(result);
    } catch (error) {
      next(error);
      res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR);
      res.json({"status" : {"code": "06", "description": "General Error"}, "errors": [{"message": "Please contact the Administrator"}]});
    }
  };

  static get = async (_, res, next) => {
    try {
      const data = await AbsentService.get();
      res.status(HTTP_STATUS_OK);
      res.json(data);
    } catch (error) {
      next(error);
    }
  };

};
