const { INVALID_REQUEST, HTTP_STATUS_OK, HTTP_STATUS_INVALID_REQUEST, CREATED, HTTP_STATUS_INTERNAL_SERVER_ERROR } = require ('../constants/Response');
const ReferenceService =  require('../services/ReferenceService');

module.exports = class ReferenceController {
  static create = async (req, res, next) => {
    try {
      const result = await ReferenceService.create(req.body);
      res.status(HTTP_STATUS_OK);
      res.json(result);
    } catch (error) {
      next(error);
      res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR);
      res.json({"status" : {"code": "06", "description": "General Error"}, "errors": [{"message": "Please contact the Administrator"}]});
    }
  };

  static get = async (req, res, next) => {
    try {
      const query = req.query;
      let data;
      if (Object.keys(query).length !== 0 && Object.keys(query.reference_type).length !== 0 && query.constructor === Object) {
        data = await ReferenceService.getAllWithReferenceType(req.query.reference_type);
      } else {
        data = await ReferenceService.getAll();
      }
      res.status(HTTP_STATUS_OK);
      res.json(data);
    } catch (error) {
      next(error);
    }
  };
};
