const { HTTP_STATUS_OK, HTTP_STATUS_INTERNAL_SERVER_ERROR } = require ('../constants/Response');

const ProductService =  require('../services/ProductService');
module.exports = class AbsentController {
  static createItem = async (req, res, next) => {
    try {
      const result = await ProductService.createItem(req.body);
      res.status(HTTP_STATUS_OK);
      res.json(result);
    } catch (error) {
      next(error);
      console.log('error when createItem : ', error);
      res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR);
      res.json({"status" : {"code": "06", "description": "General Error"}, "errors": [{"message": "Please contact the Administrator"}]});
    }
  };

  static soldItem = async (req, res, next) => {
    try {
      const result = await ProductService.soldItem(req.body);
      res.status(HTTP_STATUS_OK);
      res.json(result);
    } catch (error) {
      next(error);
      console.log('error when createItem : ', error);
      res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR);
      res.json({"status" : {"code": "06", "description": "General Error"}, "errors": [{"message": "Please contact the Administrator"}]});
    }
  };

  static get = async (_, res, next) => {
    try {
      const data = await ProductService.get();
      res.status(HTTP_STATUS_OK);
      res.json(data);
    } catch (error) {
      console.log('error when get : ', error);
      next(error);
    }
  };

};
