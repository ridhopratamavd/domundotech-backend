const { HTTP_STATUS_OK, HTTP_STATUS_INTERNAL_SERVER_ERROR } = require ('../constants/Response');

const RepairService =  require('../services/RepairService');
module.exports = class RepairController {
  static create = async (req, res, next) => {
    try {
      const result = await RepairService.create(req.body);
      res.status(HTTP_STATUS_OK);
      res.json(result);
    } catch (error) {
      next(error);
      res.status(HTTP_STATUS_INTERNAL_SERVER_ERROR);
      res.json({"status" : {"code": "06", "description": "General Error"}, "errors": [{"message": "Please contact the Administrator"}]});
    }
  };

  static get = async (_, res, next) => {
    try {
      const data = await RepairService.get();
      res.status(HTTP_STATUS_OK);
      res.json(data);
    } catch (error) {
      next(error);
    }
  };

};
