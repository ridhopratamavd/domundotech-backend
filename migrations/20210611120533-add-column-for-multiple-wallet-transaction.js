'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('StockTransactions', 'ovoIn', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'ovoOut', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'tokoPointIn', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'tokoPointOut', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'bcaIn', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'bcaOut', Sequelize.INTEGER);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('StockTransactions', 'ovoIn');
    await queryInterface.removeColumn('StockTransactions', 'ovoOut');
    await queryInterface.removeColumn('StockTransactions', 'tokoCashIn');
    await queryInterface.removeColumn('StockTransactions', 'tokoCashOut');
    await queryInterface.removeColumn('StockTransactions', 'bcaIn');
    await queryInterface.removeColumn('StockTransactions', 'bcaOut');
  }
};
