'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('StockTransactions', 'cashIn', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'cashOut', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'cashBalance', Sequelize.INTEGER);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('StockTransactions', 'cashIn');
    await queryInterface.removeColumn('StockTransactions', 'cashOut');
    await queryInterface.removeColumn('StockTransactions', 'cashBalance');
  }
};
