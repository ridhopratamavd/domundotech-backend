'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('StockTransactions', 'ovoBalance', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'tokoPointBalance', Sequelize.INTEGER);
    await queryInterface.addColumn('StockTransactions', 'bcaBalance', Sequelize.INTEGER);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('StockTransactions', 'ovoBalance');
    await queryInterface.removeColumn('StockTransactions', 'tokoPointBalance');
    await queryInterface.removeColumn('StockTransactions', 'bcaBalance');
  }
};
