'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.INTEGER
      },
      productType: {
        type: Sequelize.INTEGER
      },
      productQuantity: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('StockTransactions', 'StockTransactions_ibfk_1');
    await queryInterface.removeConstraint('StockTransactions', 'StockTransactions_ibfk_2');
    await queryInterface.removeConstraint('StockTransactions', 'StockTransactions_ibfk_3');
    await queryInterface.removeConstraint('StockTransactions', 'StockTransactions_ibfk_4');
    await queryInterface.dropTable('Products');
  }
};
