'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn(
      'Products',
      'buyPrice',
      Sequelize.INTEGER
    );
    await queryInterface.renameColumn(
      'Products',
      'price',
      'sellPrice'
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn(
      'Products',
      'buyPrice',
      Sequelize.INTEGER
    );
    await queryInterface.renameColumn(
      'Products',
      'sellPrice',
      'price'
    );
  }
};
